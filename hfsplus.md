# HFS+

## Mount read-write a HFS+ partition on Linux

HFS+ partitions can be mounted if journaling is disabled. You can disable journaling in an existing partition using the Apple Disk Utility application.

On your Mac:

- Open Disk Utility under *Applications -> Utilities*
- Select the volume to disable journaling on.
- Choose Disable Journaling from the File menu. (On later Mac OS versions you'll have to hold down the option button when you click the File menu. Or if you like `Apple+J`)

Disabling journaling from HFS+ is still possible up to OS X Yosemite 10.10

### References

- http://superuser.com/questions/84446/how-to-mount-a-hfs-partition-in-ubuntu-as-read-write
